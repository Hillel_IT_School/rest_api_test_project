package core.utils;

import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class JsonUtils {

    private JsonUtils() {
    }

    public static String readJsonFile(final File jsonFile) {
        try {
            return FileUtils.readFileToString(jsonFile);
        } catch (final IOException e) {
            throw new IllegalStateException("Unable to read the json file!", e);
        }
    }

    public static <T> String toJson(final Object object) {
        return new Gson().toJson(object);
    }

    public static <T> T fromJson(final String str, final Class<T> returnType) {
        return new Gson().fromJson(str, returnType);
    }
}
