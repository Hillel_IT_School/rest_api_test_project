import com.jayway.restassured.http.ContentType;
import core.dto.CreateNewPostDTO;
import core.dto.UserDTO;
import core.utils.JsonUtils;
import org.junit.Assert;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;

public class TestPosts extends AbstractApiTest {

    private static final String USERS_API_PATH = "/users";
    private static final String TWO_USERS_API_PATH = "/users?id=1&id=2";
    private static final String API_POSTS_ACTIONS_PATH = "/posts";
    private static final String API_ONE_POST_ACTIONS_PATH = "/posts/?id=%s";
    private static final String USERS_JSON_FILE_PATH = "./files/Users.json";

    @Test
    //GET
    public void checkApiShouldReturnAllUsers() {
        final UserDTO[] users = given()
                .get(USERS_API_PATH)
                .then()
                .statusCode(200)
                .extract()
                .as(UserDTO[].class);
        Assert.assertTrue("There are no users received!", users.length > 0);
    }

    @Test
    //GET
    public void checkApiShouldReturnSeveralUsers() {
        final File jsonFile = new File(USERS_JSON_FILE_PATH);
        final String json = JsonUtils.readJsonFile(jsonFile);
        final List<UserDTO> expectedUsers = Arrays.asList(JsonUtils.fromJson(json, UserDTO[].class));
        final UserDTO[] actualUsers = given().get(TWO_USERS_API_PATH).as(UserDTO[].class);
        ReflectionAssert.assertReflectionEquals("There are incorrect users received!",
                expectedUsers, Arrays.asList(actualUsers));
    }

    @Test
    //POST
    public void checkApiShouldCreateNewResource() {
        final CreateNewPostDTO newPost = new CreateNewPostDTO();
        newPost.setAuthor("Hillel");
        newPost.setTitle("Hillel_Test_Title");
        final String createNewPostJson = JsonUtils.toJson(newPost);
        given().contentType(ContentType.JSON)
                .body(createNewPostJson)
                .post(API_POSTS_ACTIONS_PATH);
        final CreateNewPostDTO[] createdUser = given()
                .get(String.format(API_ONE_POST_ACTIONS_PATH, 1))
                .as(CreateNewPostDTO[].class);
        ReflectionAssert.assertReflectionEquals("There is no user, created by API!",
                Collections.singletonList(newPost), Arrays.asList(createdUser));
    }

    @Test
    //DELETE
    public void checkApiShouldDeleteResource() throws UnsupportedEncodingException {
        final String url = API_POSTS_ACTIONS_PATH + "/1";
        given().delete(url);
    }

    @Test
    //PUT
    public void checkApiShouldUpdateResource() {
        final CreateNewPostDTO newPost = new CreateNewPostDTO();
        newPost.setAuthor("Hillel");
        newPost.setTitle("Hillel_Test_Title");
        final String updatePostBody = JsonUtils.toJson(newPost);
        final String url = API_POSTS_ACTIONS_PATH + "/2";
        given().contentType(ContentType.JSON)
                .body(updatePostBody)
                .put(url);
    }
}
